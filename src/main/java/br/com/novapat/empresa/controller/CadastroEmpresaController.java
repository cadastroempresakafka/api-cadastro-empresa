package br.com.novapat.empresa.controller;


import br.com.novapat.empresa.models.Empresa;
import br.com.novapat.empresa.service.CadastroEmpresaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CadastroEmpresaController {

    @Autowired
    private CadastroEmpresaService cadastroEmpresaService;

    @PostMapping("/cadastrar")
    public String cadastrarEmpresa(@RequestBody Empresa empresa) {
        String retornoCadastro = "";
        try{
            cadastroEmpresaService.create(empresa);
            retornoCadastro = "Empresa cadastrada com sucesso!";
        }catch (Exception e){
            retornoCadastro = "Erro ao cadastrar a empresa: " + e;
        }
        return retornoCadastro;
    }
}

package br.com.novapat.empresa.models;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Empresa {

    private String nome;

    private String cnpj;

    @JsonProperty("capital_social")
    private  String capitalSocial;


    public String getCapitalSocial() {
        return capitalSocial;
    }

    public void setCapitalSocial(String capitalSocial) {
        this.capitalSocial = capitalSocial;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }
}

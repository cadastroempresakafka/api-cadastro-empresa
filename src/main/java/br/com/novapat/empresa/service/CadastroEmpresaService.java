package br.com.novapat.empresa.service;

import br.com.novapat.empresa.models.Empresa;
import br.com.novapat.empresa.producer.CadastroEmpresaProducer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CadastroEmpresaService {

    @Autowired
    private CadastroEmpresaProducer cadastroEmpresaProducer;

    public void create(Empresa empresa) {
        cadastroEmpresaProducer.enviarAoKafka(empresa);
    }


}

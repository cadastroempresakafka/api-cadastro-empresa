package br.com.novapat.empresa.producer;

import br.com.novapat.empresa.models.Empresa;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class CadastroEmpresaProducer {

    @Autowired
    private KafkaTemplate<String, Empresa> producer;

    public void enviarAoKafka(Empresa empresa) {
        producer.send("spec3-patricia-novaes-2", empresa);

//        for(int i = 0; i < 30; i++) {
//            producer.send("spec2-biblioteca", i, "1", livro);
//        }
    }

}
